//
//  SignInVC.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/6/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import UIKit
import Firebase

class SignInVC: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var emailTextField: RoundTextField!

    @IBOutlet weak var passTextField: RoundTextField!
    
    var newUserPressed = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        passTextField.delegate = self

        
    }

    @IBAction func LogInBtnPressed(_ sender: Any)
    {
        if let email = emailTextField.text, let pass = passTextField.text,
        (email.characters.count > 0 && pass.characters.count > 0 )
        {
            AuthService.instance.login(email: email, pass: pass, onComplete: { (errMsg, data) in
                
                guard errMsg == nil
                else
                {
                    let alert = UIAlertController(title: "Error Authentication", message: errMsg, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
  
                }
                self.dismiss(animated: true, completion: nil)
                
            })
            
        }
        else
        {
            newUserPressed = false
            check()
        }
    }
    
    func textFieldShouldReturn(_ textfield: UITextField)-> Bool
    {
        textfield.resignFirstResponder()
        return true
    }
    
    @IBAction func createUserPressed(_ sender: Any)
    {
        newUserPressed = true
        performSegue(withIdentifier: "newUserVC", sender: nil)
        
    }
    func check()
    {
        if newUserPressed == true
        {
            
        }
        
        else
        {
            let alert = UIAlertController(title: "Username and password required", message: "Please enter a username and password", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    
}
