/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	View controller for camera interface.
*/

import UIKit
import AVFoundation
import Photos
//import Foundation


class CameraViewController: UIViewController, AVCaptureFileOutputRecordingDelegate
{
	// MARK: View Controller Life Cycle
	
    private let _session = AVCaptureSession()
    
    private var _isSessionRunning = false
    
    private let _sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
    
    private var _setupResult: SessionSetupResult = .success
    
    public var _videoDeviceInput: AVCaptureDeviceInput!
    
    public var _inProgressLivePhotoCapturesCount = 0
    
    
    public var _capturingLivePhotoLabel: UILabel!
    
    // MARK: Recording Movies
    
    public var _movieFileOutput: AVCaptureMovieFileOutput? = nil
    
    public var _backgroundRecordingID: UIBackgroundTaskIdentifier? = nil
    
    public var _recordButton: UIButton!
    
    public var _resumeButton: UIButton!
    
    public var _cameraButton: UIButton!
    
    public var _captureModeControl: UISegmentedControl!
    
    public var _cameraUnavailableLabel: UILabel!
    
    public let _photoOutput = AVCapturePhotoOutput()
    
    public var _inProgressPhotoCaptureDelegates = [Int64 : PhotoCaptureDelegate]()
    
    public var _photoButton: UIButton!
    
    private var _livePhotoMode: LivePhotoMode = .off
    
    public var _livePhotoModeButton: UIButton!
    
    var hideResume:Bool = true

    
    //var vc = CameraVC()
    
    public var _previewView: PreviewView!
    
//    var delegate: CameraVCDelegate?

    
    override func viewDidLoad()
    {
		super.viewDidLoad()
        
//        cvcd.delegate = self
		
		// Disable UI. The UI is enabled if and only if the session starts running.
        _cameraButton.isEnabled = false
        _recordButton.isEnabled = false
        _photoButton.isEnabled = false
        _livePhotoModeButton.isEnabled = false
        _captureModeControl.isEnabled = false
//      cameraButton.isEnabled = false
//		recordButton.isEnabled = false
//		photoButton.isEnabled = false
//		livePhotoModeButton.isEnabled = false
//		captureModeControl.isEnabled = false
		
		// Set up the video preview view.
		_previewView.session = _session
		
        
        
		/*
			Check video authorization status. Video access is required and audio
			access is optional. If audio access is denied, audio is not recorded
			during movie recording.
		*/
		switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
            case .authorized:
				// The user has previously granted access to the camera.
				break
			
			case .notDetermined:
				/*
					The user has not yet been presented with the option to grant
					video access. We suspend the session queue to delay session
					setup until the access request has completed.
				
					Note that audio access will be implicitly requested when we
					create an AVCaptureDeviceInput for audio during session setup.
				*/
				_sessionQueue.suspend()
				AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] granted in
					if !granted {
						self._setupResult = .notAuthorized
					}
					self._sessionQueue.resume()
				})
			
			default:
				// The user has previously denied access.
				_setupResult = .notAuthorized
		}
		
		/*
			Setup the capture session.
			In general it is not safe to mutate an AVCaptureSession or any of its
			inputs, outputs, or connections from multiple threads at the same time.
		
			Why not do all of this on the main queue?
			Because AVCaptureSession.startRunning() is a blocking call which can
			take a long time. We dispatch session setup to the sessionQueue so
			that the main queue isn't blocked, which keeps the UI responsive.
		*/
		_sessionQueue.async { [unowned self] in
			self.configureSession()
		}
        
        _previewView.session = _session

	}
    
    
	override func viewWillAppear(_ animated: Bool)
    {
		super.viewWillAppear(animated)
		
		_sessionQueue.async
            {
			switch self._setupResult
            {
                case .success:
				    // Only setup observers and start the session running if setup succeeded.
                    self.addObservers()
                    self._session.startRunning()
                    self._isSessionRunning = self._session.isRunning
				
                case .notAuthorized:
                    DispatchQueue.main.async
                        { [unowned self] in
                        let message = NSLocalizedString("AVCam doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera")
                        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                        }))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
				
                case .configurationFailed:
                    DispatchQueue.main.async
                        { [unowned self] in
                        let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                        let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
			}
		}
	}
	
	override func viewWillDisappear(_ animated: Bool)
    {
		_sessionQueue.async { [unowned self] in
			if self._setupResult == .success {
				self._session.stopRunning()
				self._isSessionRunning = self._session.isRunning
				self.removeObservers()
			}
		}
		
		super.viewWillDisappear(animated)
	}
	
    override var shouldAutorotate: Bool {
		// Disable autorotation of the interface when recording is in progress.
		if let movieFileOutput = _movieFileOutput
        {
			return !movieFileOutput.isRecording
		}
		return true
	}
	
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return .all
	}
	
	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransition(to: size, with: coordinator)
		
		if let videoPreviewLayerConnection = _previewView.videoPreviewLayer.connection
        {
			let deviceOrientation = UIDevice.current.orientation
			guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else
            {
				return
			}
			
			videoPreviewLayerConnection.videoOrientation = newVideoOrientation
		}
	}

	// MARK: Session Management
	
	private enum SessionSetupResult
    {
		case success
		case notAuthorized
		case configurationFailed
	}
	
		
	// Call this on the session queue.
	private func configureSession()
    {
		if _setupResult != .success
        {
			return
		}
		
		_session.beginConfiguration()
		
		/*
			We do not create an AVCaptureMovieFileOutput when setting up the session because the
			AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto.
		*/
		_session.sessionPreset = AVCaptureSessionPresetPhoto
		
		// Add video input.
		do {
			var defaultVideoDevice: AVCaptureDevice?
			
			// Choose the back dual camera if available, otherwise default to a wide angle camera.
			if let dualCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInDuoCamera, mediaType: AVMediaTypeVideo, position: .back)
            {
				defaultVideoDevice = dualCameraDevice
			}
			else if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back)
            {
				// If the back dual camera is not available, default to the back wide angle camera.
				defaultVideoDevice = backCameraDevice
			}
			else if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front)
            {
				// In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
				defaultVideoDevice = frontCameraDevice
			}
			
			let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice)
			
			if _session.canAddInput(videoDeviceInput)
            {
				_session.addInput(videoDeviceInput)
				self._videoDeviceInput = videoDeviceInput
				
				DispatchQueue.main.async
                    {
					/*
						Why are we dispatching this to the main queue?
						Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
						can only be manipulated on the main thread.
						Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
						on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
					
						Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
						handled by CameraViewController.viewWillTransition(to:with:).
					*/
					let statusBarOrientation = UIApplication.shared.statusBarOrientation
					var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
					if statusBarOrientation != .unknown
                    {
						if let videoOrientation = statusBarOrientation.videoOrientation
                        {
							initialVideoOrientation = videoOrientation
						}
					}
					
					self._previewView.videoPreviewLayer.connection.videoOrientation = initialVideoOrientation
				}
			}
			else
            {
				print("Could not add video device input to the session")
				_setupResult = .configurationFailed
				_session.commitConfiguration()
				return
			}
		}
		catch
        {
			print("Could not create video device input: \(error)")
			_setupResult = .configurationFailed
			_session.commitConfiguration()
			return
		}
		
		// Add audio input.
		do
        {
            let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
			let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
			
			if _session.canAddInput(audioDeviceInput)
            {
				_session.addInput(audioDeviceInput)
			}
			else
            {
				print("Could not add audio device input to the session")
			}
		}
		catch
        {
			print("Could not create audio device input: \(error)")
		}
		
		// Add photo output.
		if _session.canAddOutput(_photoOutput)
		{
			_session.addOutput(_photoOutput)
			
			_photoOutput.isHighResolutionCaptureEnabled = true
			_photoOutput.isLivePhotoCaptureEnabled = _photoOutput.isLivePhotoCaptureSupported
			_livePhotoMode = _photoOutput.isLivePhotoCaptureSupported ? .on : .off
		}
		else
        {
			print("Could not add photo output to the session")
			_setupResult = .configurationFailed
			_session.commitConfiguration()
			return
		}
		
		_session.commitConfiguration()
	}
	
	public func resumeInterruptedSession()
	{
		_sessionQueue.async { [unowned self] in
			/*
				The session might fail to start running, e.g., if a phone or FaceTime call is still
				using audio or video. A failure to start the session running will be communicated via
				a session runtime error notification. To avoid repeatedly failing to start the session
				running, we only try to restart the session running in the session runtime error handler
				if we aren't trying to resume the session running.
			*/
			self._session.startRunning()
			self._isSessionRunning = self._session.isRunning
			if !self._session.isRunning {
				DispatchQueue.main.async { [unowned self] in
					let message = NSLocalizedString("Unable to resume", comment: "Alert message when unable to resume the session running")
					let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
					let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
					alertController.addAction(cancelAction)
					self.present(alertController, animated: true, completion: nil)
				}
			}
			else {
				DispatchQueue.main.async { [unowned self] in
					self._resumeButton.isHidden = true
                    self.hideResume = true
//                    self.resumeButton.isHidden = true
				}
			}
		}
	}
	
	private enum CaptureMode: Int
    {
		case photo = 0
		case movie = 1
	}

	
	
	public func toggleCaptureMode() {
		if _captureModeControl.selectedSegmentIndex == CaptureMode.photo.rawValue {
			
            
            _recordButton.isEnabled = false
			
			_sessionQueue.async { [unowned self] in
				/*
					Remove the AVCaptureMovieFileOutput from the session because movie recording is
					not supported with AVCaptureSessionPresetPhoto. Additionally, Live Photo
					capture is not supported when an AVCaptureMovieFileOutput is connected to the session.
				*/
				self._session.beginConfiguration()
				self._session.removeOutput(self._movieFileOutput)
				self._session.sessionPreset = AVCaptureSessionPresetPhoto
				self._session.commitConfiguration()
				
				self._movieFileOutput = nil
				
				if self._photoOutput.isLivePhotoCaptureSupported
                {
					self._photoOutput.isLivePhotoCaptureEnabled = true
					
					DispatchQueue.main.async
                        {
//                        self.vc.shouldEnableLivePhotoBtn(Enabled: true)
//                        self.vc.hideLivePhotoBtn(Hide: false)
						self._livePhotoModeButton.isEnabled = true
						self._livePhotoModeButton.isHidden = false
					}
				}
			}
		}
		else if _captureModeControl.selectedSegmentIndex == CaptureMode.movie.rawValue
		{
//            vc.hideLivePhotoBtn(Hide: true)
			_livePhotoModeButton.isHidden = true
			
			_sessionQueue.async { [unowned self] in
 				let movieFileOutput = AVCaptureMovieFileOutput()
				
				if self._session.canAddOutput(movieFileOutput) {
					self._session.beginConfiguration()
					self._session.addOutput(movieFileOutput)
					self._session.sessionPreset = AVCaptureSessionPresetHigh
					if let connection = movieFileOutput.connection(withMediaType: AVMediaTypeVideo) {
						if connection.isVideoStabilizationSupported {
							connection.preferredVideoStabilizationMode = .auto
						}
					}
					self._session.commitConfiguration()
					
					self._movieFileOutput = movieFileOutput
					
					DispatchQueue.main.async { [unowned self] in
						
//                        self.vc.shouldEnableRecordUI(Enabled: true)
                        self._recordButton.isEnabled = true
					}
				}
			}
		}
	}
	
	// MARK: Device Configuration
	
		
	private let videoDeviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDuoCamera], mediaType: AVMediaTypeVideo, position: .unspecified)!
	
	  func changeCamera()
     {
//        vc.shouldEnableCameraUI(Enabled: false)
//        vc.shouldEnableRecordUI(Enabled: false)
//        vc.shouldEnablePhotoBtn(Enabled: false)
//        vc.shouldEnableLivePhotoBtn(Enabled: false)
//        vc.shouldEnableCaptureControl(Enabled: false)
		_cameraButton.isEnabled = false
		_recordButton.isEnabled = false
		_photoButton.isEnabled = false
		_livePhotoModeButton.isEnabled = false
		_captureModeControl.isEnabled = false
		
		_sessionQueue.async { [unowned self] in
			let currentVideoDevice = self._videoDeviceInput.device
			let currentPosition = currentVideoDevice!.position
			
			let preferredPosition: AVCaptureDevicePosition
			let preferredDeviceType: AVCaptureDeviceType
			
			switch currentPosition {
				case .unspecified, .front:
					preferredPosition = .back
					preferredDeviceType = .builtInDuoCamera
				
				case .back:
					preferredPosition = .front
					preferredDeviceType = .builtInWideAngleCamera
			}
			
			let devices = self.videoDeviceDiscoverySession.devices!
			var newVideoDevice: AVCaptureDevice? = nil
			
			// First, look for a device with both the preferred position and device type. Otherwise, look for a device with only the preferred position.
			if let device = devices.filter({ $0.position == preferredPosition && $0.deviceType == preferredDeviceType }).first {
				newVideoDevice = device
			}
			else if let device = devices.filter({ $0.position == preferredPosition }).first {
				newVideoDevice = device
			}

            if let videoDevice = newVideoDevice {
                do {
					let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
					
					self._session.beginConfiguration()
					
					// Remove the existing device input first, since using the front and back camera simultaneously is not supported.
		
                    self._session.removeInput(self._videoDeviceInput)
					
					if self._session.canAddInput(videoDeviceInput) {
						NotificationCenter.default.removeObserver(self, name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: currentVideoDevice!)
						
						NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
						
						self._session.addInput(videoDeviceInput)
						self._videoDeviceInput = videoDeviceInput
					}
					else {
						self._session.addInput(self._videoDeviceInput);
					}
					
					if let connection = self._movieFileOutput?.connection(withMediaType: AVMediaTypeVideo) {
						if connection.isVideoStabilizationSupported {
							connection.preferredVideoStabilizationMode = .auto
						}
					}
					
					/*
						Set Live Photo capture enabled if it is supported. When changing cameras, the
						`isLivePhotoCaptureEnabled` property of the AVCapturePhotoOutput gets set to NO when
						a video device is disconnected from the session. After the new video device is
						added to the session, re-enable Live Photo capture on the AVCapturePhotoOutput if it is supported.
					*/
					self._photoOutput.isLivePhotoCaptureEnabled = self._photoOutput.isLivePhotoCaptureSupported;
					
					self._session.commitConfiguration()
				}
				catch {
					print("Error occured while creating video device input: \(error)")
				}
			}
			
			DispatchQueue.main.async
                { [unowned self] in
//				self.vc.shouldEnableCameraUI(Enabled: true)
//                self.vc.shouldEnableRecordUI(Enabled: self._movieFileOutput != nil)
//                self.vc.shouldEnablePhotoBtn(Enabled: true)
//                self.vc.shouldEnableLivePhotoBtn(Enabled: true)
//                self.vc.shouldEnableCaptureControl(Enabled: true)
                self._cameraButton.isEnabled = true
				self._recordButton.isEnabled = self._movieFileOutput != nil
				self._photoButton.isEnabled = true
				self._livePhotoModeButton.isEnabled = true
				self._captureModeControl.isEnabled = true
			}
		}
	}
	
	@IBAction private func focusAndExposeTap(_ gestureRecognizer: UITapGestureRecognizer) {
		let devicePoint = self._previewView.videoPreviewLayer.captureDevicePointOfInterest(for: gestureRecognizer.location(in: gestureRecognizer.view))
		focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
	}
	
	private func focus(with focusMode: AVCaptureFocusMode, exposureMode: AVCaptureExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool) {
		_sessionQueue.async { [unowned self] in
			if let device = self._videoDeviceInput.device {
				do {
					try device.lockForConfiguration()
					
					/*
						Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
						Call set(Focus/Exposure)Mode() to apply the new point of interest.
					*/
					if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
						device.focusPointOfInterest = devicePoint
						device.focusMode = focusMode
					}
					
					if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
						device.exposurePointOfInterest = devicePoint
						device.exposureMode = exposureMode
					}
					
					device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
					device.unlockForConfiguration()
				}
				catch {
					print("Could not lock device for configuration: \(error)")
				}
			}
		}
	}
	
	// MARK: Capturing Photos

	
	
	public func capturePhoto() {
		/*
			Retrieve the video preview layer's video orientation on the main queue before
			entering the session queue. We do this to ensure UI elements are accessed on
			the main thread and session configuration is done on the session queue.
		*/
		let videoPreviewLayerOrientation = _previewView.videoPreviewLayer.connection.videoOrientation
		
		_sessionQueue.async {
			// Update the photo output's connection to match the video orientation of the video preview layer.
			if let photoOutputConnection = self._photoOutput.connection(withMediaType: AVMediaTypeVideo) {
				photoOutputConnection.videoOrientation = videoPreviewLayerOrientation
			}
			
			// Capture a JPEG photo with flash set to auto and high resolution photo enabled.
			let photoSettings = AVCapturePhotoSettings()
			photoSettings.flashMode = .auto
			photoSettings.isHighResolutionPhotoEnabled = true
			if photoSettings.availablePreviewPhotoPixelFormatTypes.count > 0 {
				photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String : photoSettings.availablePreviewPhotoPixelFormatTypes.first!]
			}
			if self._livePhotoMode == .on && self._photoOutput.isLivePhotoCaptureSupported { // Live Photo capture is not supported in movie mode.
				let livePhotoMovieFileName = NSUUID().uuidString
				let livePhotoMovieFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((livePhotoMovieFileName as NSString).appendingPathExtension("mov")!)
				photoSettings.livePhotoMovieFileURL = URL(fileURLWithPath: livePhotoMovieFilePath)
			}
			
			// Use a separate object for the photo capture delegate to isolate each capture life cycle.
			let photoCaptureDelegate = PhotoCaptureDelegate(with: photoSettings, willCapturePhotoAnimation: {
					DispatchQueue.main.async { [unowned self] in
						self._previewView.videoPreviewLayer.opacity = 0
						UIView.animate(withDuration: 0.25) { [unowned self] in
							self._previewView.videoPreviewLayer.opacity = 1
						}
					}
				}, capturingLivePhoto: { capturing in
					/*
						Because Live Photo captures can overlap, we need to keep track of the
						number of in progress Live Photo captures to ensure that the
						Live Photo label stays visible during these captures.
					*/
					self._sessionQueue.async { [unowned self] in
						if capturing {
							self._inProgressLivePhotoCapturesCount += 1
						}
						else {
							self._inProgressLivePhotoCapturesCount -= 1
						}
						
						let inProgressLivePhotoCapturesCount = self._inProgressLivePhotoCapturesCount
						DispatchQueue.main.async { [unowned self] in
							if inProgressLivePhotoCapturesCount > 0 {
								self._capturingLivePhotoLabel.isHidden = false
							}
							else if inProgressLivePhotoCapturesCount == 0 {
								self._capturingLivePhotoLabel.isHidden = true
							}
							else {
								print("Error: In progress live photo capture count is less than 0");
							}
						}
					}
				}, completed: { [unowned self] photoCaptureDelegate in
					// When the capture is complete, remove a reference to the photo capture delegate so it can be deallocated.
					self._sessionQueue.async { [unowned self] in
						self._inProgressPhotoCaptureDelegates[photoCaptureDelegate.requestedPhotoSettings.uniqueID] = nil
					}
				}
			)
			
			/*
				The Photo Output keeps a weak reference to the photo capture delegate so
				we store it in an array to maintain a strong reference to this object
				until the capture is completed.
			*/
			self._inProgressPhotoCaptureDelegates[photoCaptureDelegate.requestedPhotoSettings.uniqueID] = photoCaptureDelegate
			self._photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureDelegate)
		}
	}
	
	private enum LivePhotoMode {
		case on
		case off
	}
	
	
    public func toggleLivePhotoMode()
    {
		_sessionQueue.async { [unowned self] in
			
           
            self._livePhotoMode = (self._livePhotoMode == .on) ? .off : .on
			let
            livePhotoMode = self._livePhotoMode
			
			DispatchQueue.main.async
                { [unowned self] in
				if livePhotoMode == .on
                {
                    self._livePhotoModeButton.setTitle(NSLocalizedString("Live Photo Mode: On", comment: "Live photo mode button on title"), for: [])
				}
				else {
					self._livePhotoModeButton.setTitle(NSLocalizedString("Live Photo Mode: Off", comment: "Live photo mode button off title"), for: [])
				}
			}
		}
	}
	

	
	  func toggleMovieRecording()
     {
		guard let movieFileOutput = self._movieFileOutput else {
			return
		}
		
		/*
			Disable the Camera button until recording finishes, and disable
			the Record button until recording starts or finishes.
		
			See the AVCaptureFileOutputRecordingDelegate methods.
		*/
        
//        vc.shouldEnableRecordUI(Enabled: false)
//        vc.shouldEnableCameraUI(Enabled: false)
//        vc.shouldEnableCaptureControl(Enabled: false)
		_cameraButton.isEnabled = false
		_recordButton.isEnabled = false
		_captureModeControl.isEnabled = false
		
		/*
			Retrieve the video preview layer's video orientation on the main queue
			before entering the session queue. We do this to ensure UI elements are
			accessed on the main thread and session configuration is done on the session queue.
		*/
		let videoPreviewLayerOrientation = _previewView.videoPreviewLayer.connection.videoOrientation
		
		_sessionQueue.async { [unowned self] in
			if !movieFileOutput.isRecording {
				if UIDevice.current.isMultitaskingSupported {
					/*
						Setup background task.
						This is needed because the `capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)`
						callback is not received until AVCam returns to the foreground unless you request background execution time.
						This also ensures that there will be time to write the file to the photo library when AVCam is backgrounded.
						To conclude this background execution, endBackgroundTask(_:) is called in
						`capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)` after the recorded file has been saved.
					*/
					self._backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
				}
				
				// Update the orientation on the movie file output video connection before starting recording.
				let movieFileOutputConnection = self._movieFileOutput?.connection(withMediaType: AVMediaTypeVideo)
				movieFileOutputConnection?.videoOrientation = videoPreviewLayerOrientation
				
				// Start recording to a temporary file.
				let outputFileName = NSUUID().uuidString
				let outputFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((outputFileName as NSString).appendingPathExtension("mov")!)
				movieFileOutput.startRecording(toOutputFileURL: URL(fileURLWithPath: outputFilePath), recordingDelegate: self)
			}
			else {
				movieFileOutput.stopRecording()
			}
		}
	}
	
	func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
		// Enable the Record button to let the user stop the recording.
		DispatchQueue.main.async
            { [unowned self] in
//            self.vc.shouldEnableRecordUI(Enabled: true)
//            self.vc.startedRecording()
			self._recordButton.isEnabled = true
                
			self._recordButton.setTitle(NSLocalizedString("Stop", comment: "Recording button stop title"), for: [])
		}
	}
    
   
	
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
		/*
			Note that currentBackgroundRecordingID is used to end the background task
			associated with this recording. This allows a new recording to be started,
			associated with a new UIBackgroundTaskIdentifier, once the movie file output's
			`isRecording` property is back to false — which happens sometime after this method
			returns.
		
			Note: Since we use a unique file path for each recording, a new recording will
			not overwrite a recording currently being saved.
		*/
        
        
		func cleanup() {
			let path = outputFileURL.path
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                }
                catch {
                    print("Could not remove file at url: \(outputFileURL)")
                }
            }
			
			if let currentBackgroundRecordingID = _backgroundRecordingID {
				_backgroundRecordingID = UIBackgroundTaskInvalid
				
				if currentBackgroundRecordingID != UIBackgroundTaskInvalid {
					UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
				}
			}
		}
		
		var success = true
		
        var vc = CameraVC()

        
		if error != nil
        {
            vc.videoRecordingFailed()
            
			print("Movie file finishing error: \(error)")
			success = (((error as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as AnyObject).boolValue)!
		}
        
		if success
        {
			// Check authorization status.
//			PHPhotoLibrary.requestAuthorization { status in
//				if status == .authorized
//                {

                   vc.videoRecordingComplete(URL: outputFileURL as NSURL)
            
            
                    
                    // *****we do not want to save video to photo library since we are sending a video and not wanting anyone else to see it after the watch it for set period of time.
                    
                    
                    
                    
                    
                    
//					// Save the movie file to the photo library and cleanup.
//					PHPhotoLibrary.shared().performChanges({
//							let options = PHAssetResourceCreationOptions()
//							options.shouldMoveFile = true
//							let creationRequest = PHAssetCreationRequest.forAsset()
//							creationRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
//						}, completionHandler: { success, error in
//							if !success {
//								print("Could not save movie to photo library: \(error)")
//							}
//							cleanup()
//						}
//					)
				}
//				else {
//					cleanup()
//				}
//			}
		
        else
        {
			cleanup()
		}
    
		// Enable the Camera and Record buttons to let the user switch camera and start another recording.
		DispatchQueue.main.async { [unowned self] in
			// Only enable the ability to change camera if the device has more than one camera.
			
//            self.vc.shouldEnableCameraUI(Enabled: self.videoDeviceDiscoverySession.uniqueDevicePositionsCount() > 1)
//            self.vc.shouldEnableRecordUI(Enabled: true)
//            self.vc.shouldEnableCaptureControl(Enabled: true)
//            self.vc.canStartRecording()
            self._cameraButton.isEnabled = self.videoDeviceDiscoverySession.uniqueDevicePositionsCount() > 1
			self._recordButton.isEnabled = true
			self._captureModeControl.isEnabled = true
			self._recordButton.setTitle(NSLocalizedString("Record", comment: "Recording button record title"), for: [])
        }}
	
	// MARK: KVO and Notifications
	
	private var sessionRunningObserveContext = 0
	
	private func addObservers() {
		_session.addObserver(self, forKeyPath: "running", options: .new, context: &sessionRunningObserveContext)
		
		NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: _videoDeviceInput.device)
		NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: Notification.Name("AVCaptureSessionRuntimeErrorNotification"), object: _session)
		
		/*
			A session can only run when the app is full screen. It will be interrupted
			in a multi-app layout, introduced in iOS 9, see also the documentation of
			AVCaptureSessionInterruptionReason. Add observers to handle these session
			interruptions and show a preview is paused message. See the documentation
			of AVCaptureSessionWasInterruptedNotification for other interruption reasons.
		*/
		NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: Notification.Name("AVCaptureSessionWasInterruptedNotification"), object: _session)
		NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: Notification.Name("AVCaptureSessionInterruptionEndedNotification"), object: _session)
	}
	
	private func removeObservers() {
		NotificationCenter.default.removeObserver(self)
		
		_session.removeObserver(self, forKeyPath: "running", context: &sessionRunningObserveContext)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if context == &sessionRunningObserveContext {
			let newValue = change?[.newKey] as AnyObject?
			guard let isSessionRunning = newValue?.boolValue else { return }
			let isLivePhotoCaptureSupported = _photoOutput.isLivePhotoCaptureSupported
			let isLivePhotoCaptureEnabled = _photoOutput.isLivePhotoCaptureEnabled
			
			DispatchQueue.main.async { [unowned self] in
				// Only enable the ability to change camera if the device has more than one camera.
//                self.vc.shouldEnableRecordUI(Enabled: isSessionRunning && self._movieFileOutput != nil)
//                self.vc.shouldEnableCameraUI(Enabled: isSessionRunning && self.videoDeviceDiscoverySession.uniqueDevicePositionsCount() > 1)
//                self.vc.shouldEnablePhotoBtn(Enabled: isSessionRunning)
//                self.vc.shouldEnableCaptureControl(Enabled: isSessionRunning)
//                self.vc.shouldEnableLivePhotoBtn(Enabled: isSessionRunning && isLivePhotoCaptureEnabled)
//                self.vc.hideLivePhotoBtn(Hide: !(isSessionRunning && isLivePhotoCaptureSupported))
//                
				self._cameraButton.isEnabled = isSessionRunning && self.videoDeviceDiscoverySession.uniqueDevicePositionsCount() > 1
				self._recordButton.isEnabled = isSessionRunning && self._movieFileOutput != nil
				self._photoButton.isEnabled = isSessionRunning
				self._captureModeControl.isEnabled = isSessionRunning
				self._livePhotoModeButton.isEnabled = isSessionRunning && isLivePhotoCaptureEnabled
				self._livePhotoModeButton.isHidden = !(isSessionRunning && isLivePhotoCaptureSupported)
			}
		}
		else {
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
	func subjectAreaDidChange(notification: NSNotification) {
		let devicePoint = CGPoint(x: 0.5, y: 0.5)
		focus(with: .autoFocus, exposureMode: .continuousAutoExposure, at: devicePoint, monitorSubjectAreaChange: false)
	}
	
	func sessionRuntimeError(notification: NSNotification) {
		guard let errorValue = notification.userInfo?[AVCaptureSessionErrorKey] as? NSError else {
			return
		}
		
        let error = AVError(_nsError: errorValue)
		print("Capture session runtime error: \(error)")
		
		/*
			Automatically try to restart the session running if media services were
			reset and the last start running succeeded. Otherwise, enable the user
			to try to resume the session running.
		*/
		if error.code == .mediaServicesWereReset {
			_sessionQueue.async { [unowned self] in
				if self._isSessionRunning {
					self._session.startRunning()
					self._isSessionRunning = self._session.isRunning
				}
				else {
					DispatchQueue.main.async { [unowned self] in
//                        self.vc.hideResumeBtn(Hide: false)
                        self.hideResume = false
						self._resumeButton.isHidden = false
					}
				}
			}
		}
		else {
//            self.vc.hideResumeBtn(Hide: false)
            self.hideResume = false
            _resumeButton.isHidden = false
		}
	}
	
	func sessionWasInterrupted(notification: NSNotification) {
		/*
			In some scenarios we want to enable the user to resume the session running.
			For example, if music playback is initiated via control center while
			using AVCam, then the user can let AVCam resume
			the session running, which will stop music playback. Note that stopping
			music playback in control center will not automatically resume the session
			running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
		*/
		if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?, let reasonIntegerValue = userInfoValue.integerValue, let reason = AVCaptureSessionInterruptionReason(rawValue: reasonIntegerValue) {
			print("Capture session was interrupted with reason \(reason)")
			
			var showResumeButton = false
			
			if reason == AVCaptureSessionInterruptionReason.audioDeviceInUseByAnotherClient || reason == AVCaptureSessionInterruptionReason.videoDeviceInUseByAnotherClient {
				showResumeButton = true
			}
			else if reason == AVCaptureSessionInterruptionReason.videoDeviceNotAvailableWithMultipleForegroundApps {
				// Simply fade-in a label to inform the user that the camera is unavailable.
				_cameraUnavailableLabel.alpha = 0
				_cameraUnavailableLabel.isHidden = false
				UIView.animate(withDuration: 0.25) { [unowned self] in
					self._cameraUnavailableLabel.alpha = 1
				}
			}
			
			if showResumeButton {
				// Simply fade-in a button to enable the user to try to resume the session running.
//                vc.hideResumeBtn(Hide: false)
                hideResume = false
//                vc.resumeBtnAlpha(Alpha: 0)
				_resumeButton.alpha = 0
				_resumeButton.isHidden = false
				UIView.animate(withDuration: 0.25)
                { [unowned self] in
//                    self.vc.resumeBtnAlpha(Alpha: 1)
					self._resumeButton.alpha = 1
				}
			}
		}
	}
	
	func sessionInterruptionEnded(notification: NSNotification) {
		print("Capture session interruption ended")
		
		if hideResume != true {
			UIView.animate(withDuration: 0.25,
				animations: { [unowned self] in
                    
//                    self.vc.resumeBtnAlpha(Alpha: 0)
					self._resumeButton.alpha = 0
				}, completion: { [unowned self] finished in
//                    self.vc.hideResumeBtn(Hide: true)
                    self.hideResume = true
					self._resumeButton.isHidden = true
				}
			)
		}
		if !_cameraUnavailableLabel.isHidden {
			UIView.animate(withDuration: 0.25,
			    animations: { [unowned self] in
					self._cameraUnavailableLabel.alpha = 0
				}, completion: { [unowned self] finished in
					self._cameraUnavailableLabel.isHidden = true
				}
			)
		}
	}
}

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
            case .portrait: return .portrait
            case .portraitUpsideDown: return .portraitUpsideDown
            case .landscapeLeft: return .landscapeRight
            case .landscapeRight: return .landscapeLeft
            default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
            case .portrait: return .portrait
            case .portraitUpsideDown: return .portraitUpsideDown
            case .landscapeLeft: return .landscapeLeft
            case .landscapeRight: return .landscapeRight
            default: return nil
        }
    }
}

extension AVCaptureDeviceDiscoverySession {
	func uniqueDevicePositionsCount() -> Int {
		var uniqueDevicePositions = [AVCaptureDevicePosition]()
		
		for device in devices {
			if !uniqueDevicePositions.contains(device.position) {
				uniqueDevicePositions.append(device.position)
			}
		}
		
		return uniqueDevicePositions.count
	}
}
