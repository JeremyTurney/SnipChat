//
//  CameraDelegateVC.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/15/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import AVFoundation

protocol CameraDelegateVC
{
    func videoRecordingComplete(URL:NSURL)
    func videoRecordingFailed()
    func snapshotFailed()
    func snapshotTaken(snapshotData: Data!)


}
