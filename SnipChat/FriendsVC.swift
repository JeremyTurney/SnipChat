//
//  FriendsVC.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/13/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import UIKit
import Firebase

class FriendsVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var tableView: UITableView!
    
    private var users = [User]()
    private var selectedUsers = Dictionary<String, User>()
    
    private var _snapData: Data?
    private var _videoURL: URL?
    
    var snapData:Data?
    {
        set
        {
            _snapData = newValue
        }
        get
        {
            return _snapData
        }
    }
    
    var videoURL: URL?
    {
        set
        {
            _videoURL = newValue
        }
        get
        {
            return _videoURL
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsMultipleSelection = true
        
        DataService.instance.usersRef.observeSingleEvent(of: .value)
        { (snapshot: FIRDataSnapshot) in
            
            if let users = snapshot.value as? Dictionary<String, AnyObject>
            {
                for (key,value) in users
                {
                    if let dict = value as? Dictionary<String, AnyObject>
                    {
                        if let profile = dict["profile"] as? Dictionary<String, AnyObject>
                        {
                            if let firstName = profile["firstName"] as? String
                            {
                                let uid = key
                                let user = User(uid: uid, firstName: firstName)
                                self.users.append(user)
                            }
                        }
                    }
                }
            }
            
            self.tableView.reloadData()
            print("Jeremy: users = \(self.users)")
            
        }
            
        

        // Do any additional setup after loading the view.
    }

    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath) as! UserCell
        cell.setCheckMark(selected: true)
        let user = setUser(indexPath: indexPath)
        selectedUsers[user.uid] = user
        
        navigationItem.rightBarButtonItem?.isEnabled = true
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath) as! UserCell
        cell.setCheckMark(selected: false)
        let user = setUser(indexPath: indexPath)
        selectedUsers[user.uid] = nil
        
        if selectedUsers.count <= 0
        {
            navigationItem.backBarButtonItem?.isEnabled = false
        }
        else
        {
            navigationItem.backBarButtonItem?.isEnabled = true
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserCell
        //let user = users[indexPath.row]
        
        cell.updateUI(user: setUser(indexPath: indexPath))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return users.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func setUser(indexPath: IndexPath) -> User
    {
        let user = users[indexPath.row]
        
        return user
    }
    
    
    @IBAction func SendSnipPressed(_ sender: Any)
    {
        if let url = videoURL
        {
            let videoName = "\(NSUUID().uuidString)\(url)"
            let ref = DataService.instance.videoStorageRef.child(videoName)
            
            _ = ref.putFile(url, metadata: nil, completion: { (meta:FIRStorageMetadata?, err: Error?) in
                
                if err != nil
                {
                    print("Jeremy: Error uploading video: \(err?.localizedDescription)")
                }
                else
                {
                    let downloadURL = meta!.downloadURL()
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
        }
        else if let snap = snapData
        {
            let snapName = "\(NSUUID().uuidString).jpeg"
            let ref = DataService.instance.imageStoreRef.child(snapName)
            
            _ = ref.put(snap, metadata: nil, completion: { (meta:FIRStorageMetadata?, err: Error?) in
                
                if err != nil
                {
                    print("Jeremy: Error uploading photo: \(err?.localizedDescription)")
                }
                else
                {
                    let downloadURL = meta!.downloadURL()
                    self.dismiss(animated: true, completion: nil)
                }
                
            })
        }
        
        
        
    }
    
    
}
