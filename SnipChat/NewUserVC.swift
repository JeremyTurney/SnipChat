//
//  NewUserVC.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/9/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import UIKit
import Firebase

class NewUserVC: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var email1TextField: RoundTextField!

    @IBOutlet weak var email2TextField: RoundTextField!
    
    @IBOutlet weak var pass1TextField: RoundTextField!
    
    @IBOutlet weak var pass2TextField: RoundTextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        email1TextField.delegate = self
        email2TextField.delegate = self
        pass1TextField.delegate = self
        pass2TextField.delegate = self
        
    }

    @IBAction func CreateUserPressed(_ sender: Any)
    {
        if email1TextField.text == email2TextField.text
        {
            if pass1TextField.text == pass2TextField.text
            {
                AuthService.instance.createUser(email: email1TextField.text!, pass: pass1TextField.text!, onComplete: { (errMsg, data) in
                    
                    guard errMsg == nil
                        else
                    {
                        let alert = UIAlertController(title: "Error Authentication", message: errMsg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return
                        
                    }
                    self.dismiss(animated: true, completion: nil)
                    
                })
                
            
            }
            else
            {
                textFieldShouldClear(pass1TextField)
                textFieldShouldClear(pass2TextField)
                let alert = UIAlertController(title: "Passwords do not match", message: "Please enter the correct passwords", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            textFieldShouldClear(email1TextField)
            textFieldShouldClear(email2TextField)
            let alert = UIAlertController(title: "Emails do not match", message: "Please enter emails that match", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func BackPressed(_ sender: Any)
    {
        dismiss(animated: false, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        textField.text = ""
        return true
    }
   
}
