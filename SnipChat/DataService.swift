//
//  DataService.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/12/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

let FIR_CHILD_USERS = "users"
typealias CompletionHandler = (_ Success: Bool) -> Void

func isSuccess(data: Data!, completion: @escaping CompletionHandler)
{
    if data != nil
    {
        completion(true)
        print("Jeremy: completion status \(completion)")
    }
    else
    {
        completion(false)
        print("Jeremy: completion status \(completion)")

    }
}
    
    
    



class DataService
{
    private static let _instance = DataService()
    
    static var instance:DataService
    {
        return _instance
    }
    
    var mainRef: FIRDatabaseReference
    {
        return FIRDatabase.database().reference()
    }
    
    var mainStorageRef: FIRStorageReference
    {
        return FIRStorage.storage().reference(forURL: "gs://snipchat-d9b01.appspot.com")
    }
    
    var imageStoreRef: FIRStorageReference
    {
        return mainStorageRef.child("images")
    }
    
    var videoStorageRef: FIRStorageReference
    {
        return mainStorageRef.child("videos")
    }
    
    var usersRef:FIRDatabaseReference
    {
        return mainRef.child(FIR_CHILD_USERS)
    }
    
    func saveUser(_ userID:String)
    {
        let profile: Dictionary<String, AnyObject> = ["firstName":"" as AnyObject, "lastName" : "" as AnyObject]
        mainRef.child(FIR_CHILD_USERS).child(userID).child("profile").setValue(profile)
    }
}
