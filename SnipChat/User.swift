//
//  User.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/13/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import UIKit

struct User
{
    private var _firstName:String
    private var _uid:String
    
    var firstname:String
    {
        return _firstName
    }
    
    var uid:String
    {
        return _uid
    }
    
    init(uid:String, firstName:String)
    {
        _uid = uid
        _firstName = firstName
    }
    
}
