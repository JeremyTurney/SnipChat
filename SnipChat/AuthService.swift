//
//  AuthService.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/9/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

typealias Completion = (_ errMsg:String?, _ data: AnyObject?) -> Void



class AuthService
{
    private static let _instance = AuthService()
    
    static var instance: AuthService
    {
        return _instance
    }
    
    func login (email: String, pass: String, onComplete: Completion?)
    {
        FIRAuth.auth()?.signIn(withEmail: email, password: pass, completion: {(user, error) in
            
            if error != nil
            {
                if let errorCode = FIRAuthErrorCode(rawValue: (error?._code)!)
                {
                    if errorCode == .errorCodeUserNotFound
                    {
                        
                        // let user know they need to create a profile
                        
                        self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                        
//                        let alert = UIAlertController(title: "User does not exist", message: "Please create a user account", preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
//                        present(alert, animated: true, completion: nil)

                    }
                    else
                    {
                        self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                    }
                }
                else
                {
                    print("Jeremy: errorCode could not be set to FIRAuth")
                }
            }
            else
            {
                // let user know sign in successful
                
                onComplete?(nil, user)
            }
            
        })
        
        
        
    }
    
    func createUser(email:String, pass: String, onComplete: Completion?)
    {
        FIRAuth.auth()?.createUser(withEmail: email, password: pass, completion: {(user, error) in
            
            if error != nil
            {
                self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                //output error
            }
            else
            {
                if user?.uid != nil
                {
                    DataService.instance.saveUser(user!.uid) // save user
                    
                    FIRAuth.auth()?.signIn(withEmail: email, password: pass, completion: {(user, error) in
                        
                        if error != nil
                        {
                            self.handleFirebaseError(error: error! as NSError, onComplete: onComplete)
                        }
                        else
                        {
                            //user created and signed in successful
                            onComplete?(nil, user)
                        }
                        
                        
                    })
                }
                else
                {
                    onComplete?(nil, user)
                }
                
            }
        })

        
    }
    
    func handleFirebaseError (error: NSError, onComplete: Completion?)
    {
        print (error.debugDescription)
        
        if let errorCode = FIRAuthErrorCode(rawValue: error._code)
        {
            switch (errorCode)
            {
            case .errorCodeInvalidEmail:
                onComplete?("Invalid email address", nil)
                break
                
            case .errorCodeWrongPassword:
                onComplete?("Invalid password", nil)
                break
            
            case .errorCodeEmailAlreadyInUse, .errorCodeAccountExistsWithDifferentCredential:
                onComplete?("Email already in use", nil)
                break
            
            case .errorCodeUserNotFound:
                onComplete?("No user account found. Please create a user account", nil)
                break
            
            default:
                onComplete?("There was an issue autheticating. Please try again.", nil)

            }
        }
    }
    
}
