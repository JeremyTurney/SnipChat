//
//  CameraVC.swift
//  SnipChat
//
//  Created by Jeremy Turney on 11/29/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import UIKit
import AVFoundation
import FirebaseAuth

class CameraVC: CameraViewController, CameraDelegateVC
{

    @IBOutlet var previewView: PreviewView!
    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var livePhotoModeBtn: UIButton!
    @IBOutlet weak var resumeBtn: UIButton!
    @IBOutlet weak var photoBtn: UIButton!
    @IBOutlet weak var cameraLbl: UILabel!
    @IBOutlet weak var captureModeControl: UISegmentedControl!
    
    override func viewDidLoad()
    {
        self._previewView = previewView
        _cameraButton = cameraBtn
        _recordButton = recordBtn
        _cameraUnavailableLabel = cameraLbl
        _captureModeControl = captureModeControl
        _resumeButton = resumeBtn
        _photoButton = photoBtn
        _livePhotoModeButton = livePhotoModeBtn
        
        
        super.viewDidLoad()
        
        
       // self._previewView = previewView

        // Do any additional setup after loading the view, typically from a nib.
        //self._previewView = previewView
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        guard FIRAuth.auth()?.currentUser != nil
        else
        {
          //load login VC
            performSegue(withIdentifier: "SignInVC", sender: nil)
            return
        }
    }

    @IBAction func recordBtnPressed(_ sender: Any)
    {
        toggleMovieRecording()
    }
    
    @IBAction func changeCameraBtnPressed(_ sender: Any)
    {
        changeCamera()
    }

    @IBAction func livePhotoModePressed(_ sender: Any)
    {
        toggleLivePhotoMode()
    }
  
    @IBAction func resumePressed(_ sender: Any)
    {
        resumeInterruptedSession()
    }

    @IBAction func photoBtnPressed(_ sender: Any)
    {
        capturePhoto()
    }
    
    @IBAction func changeCaptureMode(_ sender: UISegmentedControl)
    {
        toggleCaptureMode()

    }
    
    public func videoRecordingComplete(URL: NSURL)
    {
        if URL != nil
        {
            performTransition(success: true, data: nil, url: URL)
        }

    }
    
    func videoRecordingFailed()
    {
        
    }
    
    func snapshotFailed()
    {
        
    }
    
    public func snapshotTaken(snapshotData: Data!)
    {
        
        print("Jeremy: Data = \(snapshotData)")
        
        if snapshotData != nil
        {
            performTransition(success: true, data: snapshotData, url: nil)
        }
        else
        {
            
        }
        
        //performSegue(withIdentifier: "SignInVC", sender:nil) //snapshotData)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let friendVC = segue.destination as? FriendsVC
        {
            if let videoDict = sender as? Dictionary<String, URL>
            {
                let url = videoDict["videoURL"]
                friendVC.videoURL = url
            }
            else if let snapDict = sender as? Dictionary<String, Data>
            {
                let data = snapDict["snapshotData"]
                friendVC.snapData = data
            }
            
        }
    }
    
    func performTransition(success: Bool, data: Data?, url: NSURL?)
    {
        if success == true && data != nil
        {
            performSegue(withIdentifier: "FriendsVC", sender:data) //snapshotData)
        }
        else if success == true && url != nil
        {
            performSegue(withIdentifier: "FriendsVC", sender:url) //snapshotData)
        }
        else
        {
            print("Jeremy: Unsuccessful Data = \(data)")
        }
    }

}

