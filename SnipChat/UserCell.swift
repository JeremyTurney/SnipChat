//
//  UserCell.swift
//  SnipChat
//
//  Created by Jeremy Turney on 12/13/16.
//  Copyright © 2016 Jeremy Turney. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell
{
    @IBOutlet weak var firstNameLbl: UILabel!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        setCheckMark(selected: false)
    }
    
    func updateUI(user: User)
    {
        firstNameLbl.text = user.firstname
    }

    func setCheckMark(selected: Bool)
    {
        let imageStr = selected ?  "messageindicatorchecked1"  :"messageindicator1"
        self.accessoryView = UIImageView(image: UIImage(named: imageStr))
        
    }
}
